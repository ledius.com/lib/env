import { Module } from '@nestjs/common';
import { EnvProviderService } from './services/env-provider.service';

@Module({
  providers: [
    {
      provide: EnvProviderService,
      useValue: new EnvProviderService(process.env),
    },
  ],
  exports: [EnvProviderService],
})
export class EnvModule {}
