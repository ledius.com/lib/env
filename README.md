[NPM Package](https://www.npmjs.com/package/@ledius/env)

### Installation

`npm i -S @ledius/env`

### Import into module

```typescript
import { EnvModule } from "@ledius/env";

@Module({
    imports: [EnvModule]
})
export class AppModule {
}
```

### Inject into service

```typescript
import {EnvProviderService} from "@ledius/env";

export class AppService {
    constructor(
        private readonly envProviderService: EnvProviderService
    ) {}
}
```

### Get env variables in methods

```typescript
import {EnvProviderService} from "@ledius/env";

export class AppService {
    constructor(
        private readonly envProviderService: EnvProviderService
    ) {}
    
    public myMethod() {
        // returns string or undefined
        const appName = this.envProviderService.get('APP_NAME');
        
        // returns string or failed
        const apiUrl = this.envProviderService.getOrFail('APP_NAME');
        
        // get all envs
        const envs = this.envProviderService.getAll();
    }
}
```


### Best practice

Define a enum to get envs

```typescript
import {EnvProviderService} from "@ledius/env";

export enum LocalEnvPaths {
    APP_NAME = 'APP_NAME'
}

export class AppService {
    public myMethod() {
        // returns string or undefined
        const appName = this.envProviderService.get(LocalEnvPaths.APP_NAME);
    }
}
```
